#!/bin/bash

set -efo pipefail

# renovate: datasource=github-releases depName=redis/redis
VERSION=7.0.4
